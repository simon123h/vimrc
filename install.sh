#!/bin/bash

# check for active network connection
if ! ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  echo "Error: no network connection."
  exit 1
fi

# if "-q" was passed --> quiet mode
if [ "$1" = "-q" ]; then
  quiet_wget=-q
fi

# link .vimrc file and .vim folder
rm -rf ~/.vimrc
ln -sTfv "$(pwd -P)/.vimrc" "$HOME/.vimrc"
rm -rf ~/.vim
ln -sTfv "$(pwd -P)" "$HOME/.vim"

# install plugin manager vim-plug
mkdir -p ~/.vim/autoload
wget $quiet_wget -nc -O ~/.vim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install powerline font
mkdir -p ~/.local/share/fonts
(cd ~/.local/share/fonts &&
    wget $quiet_wget -nc -O "Droid Sans Mono Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf)


# install plugins via vim-plug
echo "Installing vim plugins"
if [ "$1" = "-q" ]; then
  vim +PlugInstall +qall > /dev/null
else
  vim +PlugInstall +qall
fi
