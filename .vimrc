"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                             "
"                      VIM CONFIGURATION                      "
"                                                             "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Disable downwards compitability to Vi, could otherwise introduce errors
set nocompatible

" encoding
set encoding=utf-8

" Confirm before exit without saving
set confirm

" Enable use of the mouse for all modes
set mouse=a

" Minimize buffers to background instead of closing them
set hidden

" Use 'unix' as the standard file system type
set ffs=unix,dos,mac

" Enable file type detection
filetype plugin on

" Reload file when it is changed from the outside
set autoread

" Do not create .swp files or backups
set noswapfile
set nobackup
set nowritebackup

" command & undo history length
set history=100
set undolevels=1000

" Display line numbers
set number

" Always display signcolumn used for errors and git-gutter
silent! set signcolumn=yes
" Add a bit extra margin to the left, looks better with signcolumn
" set foldcolumn=1

" Always display the status line at the bottom of the window
set laststatus=2

" Always display line number/position info
set ruler

" Highlight current line
set cursorline

" Display open commands in the lower right corner
set showcmd

" Configure menu for command-line completion
set wildmenu
set wildmode=list:longest,full

" Show matching brackets when text indicator is over them
set showmatch

" wrap long lines
set wrap

" prefer spaces over tabs, smart tab detection, 1 tab = 4 spaces
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" adaptive indentation based on surrounding code
set autoindent
" set smartindent

" Search: instant (incremental) search, highlight matches and smart case-sensitivity
set incsearch
set hlsearch
set ignorecase
set smartcase

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Where to open splits when openend?
set splitright
set splitbelow

" Current file title is window title
set title

" jump to next line if line end is reached
set whichwrap+=<,>,h,l,[,]

" let backspace jump over some characters
set backspace=eol,start,indent

" Try to use the system clipboard?
" set clipboard+=unnamed

" Speed optimization
set lazyredraw
autocmd VimEnter * redrawstatus! " Fix black status line error

" Empty search register, Fixes mishighlighted words on .vimrc load
let @/=''

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Install VimPlug plugin manager using curl, if it isn't
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Auto-install missing plugins
autocmd VimEnter *
            \  if !empty(filter(copy(g:plugs), '!isdirectory(v:val.dir)'))
            \|   PlugInstall --sync | q
            \| endif

" include plugins
call plug#begin('~/.vim/plugged')

" color schemes
Plug 'joshdick/onedark.vim'
" Plug 'rakr/vim-one'
" Plug 'sonph/onehalf', {'rtp': 'vim/'}
" Plug 'arcticicestudio/nord-vim'
" Plug 'chriskempson/base16-vim'

" status bar & tabline
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
set showtabline=2
let g:lightline = {
            \ 'colorscheme': 'onedark',
            \  'tabline': {'left': [['buffers']], 'right': [['tabind']]},
            \ 'component_expand': {'buffers': 'lightline#bufferline#buffers', 'tabind': 'TabIndicator'},
            \ 'component_type': {'buffers': 'tabsel'},
            \ 'component_raw': {'buffers': 1},
            \ 'tabline_separator': {'left': ' ', 'right': ''},
            \ 'tabline_subseparator': {'left': '⎸', 'right': ''}
            \ }
let g:lightline#bufferline#margin_right = 1
let g:lightline#bufferline#clickable = 1
let g:lightline#bufferline#enable_devicons = 1
" let g:lightline#bufferline#filename_modifier = ':t'
" let g:lightline#bufferline#unicode_symbols = 1
" for showing tab number in top right corner
function! TabIndicator() abort
    let ntabs = tabpagenr('$')
    if ntabs > 1
        return 'tab '.tabpagenr().'/'.ntabs.''
    endif
    return ''
endfunction

" file icons
Plug 'ryanoasis/vim-devicons'

" file tree
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }
let NERDTreeIgnore=['\.DS_Store', '\~$', '\.git', '\.swp', '\.swo', '\.pyc$', '__pycache__']
let NERDTreeQuitOnOpen=1

" Fuzzy finder for files etc.
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Smart buffer closing
Plug 'mhinz/vim-sayonara'

" Switch windows with the same keys as tmux: Ctrl+h/j/k/l
Plug 'christoomey/vim-tmux-navigator'

" Indentation indicator lines
Plug 'Yggdroot/indentLine'
if $TERM != "linux"
    let g:indentLine_char = '⎸'
endif
let g:indentLine_fileTypeExclude = ['markdown']

" Easy commenting in/out
Plug 'tpope/vim-commentary'

" Auto-close brackets
Plug 'tpope/vim-surround'
" Plug 'LunarWatcher/auto-pairs'
" let g:AutoPairsCompleteOnlyOnSpace = 1
" let g:AutoPairsMapBS = 1

" Multi-cursor
" Activate with Ctrl+d, then use visual mode commands
if v:version >= 800
    Plug 'mg979/vim-visual-multi/'
    let g:VM_default_mappings = 0
    let g:VM_mouse_mappings = 1
    let g:VM_maps = {}
    let g:VM_maps['Find Under']         = '<C-d>'  " replacing C-n
    let g:VM_maps['Find Subword Under'] = '<C-d>'  " replacing visual C-n
endif

" Make search behave more sane
Plug 'junegunn/vim-slash'

" Git
" Plug 'tpope/vim-fugitive'
if has('nvim') || has('patch-8.0.902')
    Plug 'mhinz/vim-signify'
else
    Plug 'mhinz/vim-signify', { 'branch': 'legacy' }
endif   
set updatetime=300
let g:signify_sign_change = '│'
let g:signify_sign_add = '│'
let g:signify_sign_show_count = 0

" Enhanced syntax highlighting / language support
Plug 'sheerun/vim-polyglot'

" LaTeX
Plug 'lervag/vimtex', {'for': ['tex', 'latex']}
" dont convert latex symbols to actual symbols
let g:vimtex_syntax_conceal_default = 0

call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Appearance and colorscheme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable syntax highlighting
if has('syntax') && !exists('g:syntax_on')
    syntax enable
endif

" Enable 256 colors
set t_Co=256

" Disable background color erase
set t_ut=

" Try to enable true colors
if (has("termguicolors"))
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
    " Disable for tty terminals
    if $TERM == "linux" || $TERM == "screen" || $TERM == "screen.xterm-256color"
        set notermguicolors
    endif
endif

" Fix black background for 16-color terminals
let g:onedark_color_overrides = {"black": {"gui": "#24272E", "cterm": "232", "cterm16": "0" }}

" Set colorscheme
silent! colorscheme onedark
set background=dark
" make background translucent, so that we see the terminals background color
hi Normal guibg=NONE ctermbg=NONE

" Tweak the lightline/tabline colors
silent! let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
silent! let s:palette.tabline.tabsel = [['#000000', '#98C379', 232, 114]]
silent! let s:palette.normal.middle = [['#5C6370', 'NONE', 59, 'NONE']]
silent! let s:palette.tabline.middle = s:palette.normal.middle
silent! let s:palette.inactive.middle = s:palette.normal.middle
silent! unlet s:palette

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Keybindings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" With a map leader it's possible to do extra key combinations
let mapleader = " "

" remap jj to esc key for quick usage
" inoremap jj <Esc>
" vnoremap jj <Esc>
" if has("nvim")
"     tmap jj <Esc> <C-\><C-n>
" endif
" Alternative choice: kj
inoremap kj <Esc>
vnoremap kj <Esc>
if has("nvim")
    tmap kj <Esc> <C-\><C-n>
endif

" <leader>+w: close current buffer
noremap <silent> <leader>w :Sayonara!<CR>
" <leader>+q: close current buffer or window
noremap <silent> <leader>q :q<CR>
" noremap <silent> <leader>q :Sayonara<CR>
" <leader>+s: save file
noremap <silent> <leader>s :update<CR>
" Ctrl+s: save file
noremap <silent> <C-s> :update<CR>
inoremap <silent> <C-s> <Esc>:update<CR>
vnoremap <silent> <C-s> <Esc>:update<CR>
" Ctrl+t: toggle file tree
map <silent> <C-n> :NERDTreeToggle<CR>
" Ctrl+p: fuzzy find files using fzf
map <silent> <C-p> :Files<CR>
" F1: fuzzy find commands using fzf
map <silent> <F1> :Commands<CR>
" <leader>+b: search buffers using fzf
nnoremap <leader>b :Buffers<CR>
" Ctrl+f: search current file using fzf
nnoremap <C-f> :BLines <CR>
" Find in all files using ripgrep
nnoremap ff :Rg <CR>
" F3: toggle paste mode
set pastetoggle=<F3>
" F4: toggle line number modes
nnoremap <F4> :exe 'set nu!' &nu ? 'rnu!' : ''<CR>
" Run current file with <F5>, if it is executable
nnoremap <F5> :!cd "%:p:h"; "./%:t"<CR>

" text selection shortcuts: arrow keys and backspace
nnoremap <S-up> v<Up>
nnoremap <S-Down> v<Down>
nnoremap <S-Left> v<Left>
nnoremap <S-Right> v<Right>
vnoremap <S-Up> <Up>
vnoremap <S-Down> <Down>
vnoremap <S-Left> <Left>
vnoremap <S-Right> <Right>
inoremap <S-Up> <Esc>v<Up>
inoremap <S-Down> <Esc><Right>v<Down>
inoremap <S-Left> <Esc>v
inoremap <S-Right> <Esc><Right>v
vnoremap <Backspace> "_d

" mark all, copy, cut and paste
" Ctrl+A
noremap <C-a> gggH<C-O>G
inoremap <C-a> <C-O>gg<C-O>gH<C-O>G
cnoremap <C-a> <C-C>gggH<C-O>G
onoremap <C-a> <C-C>gggH<C-O>G
snoremap <C-a> <C-C>gggH<C-O>G
xnoremap <C-a> <C-C>ggVG
" Ctrl+C, Ctrl+X, Ctrl+V
vnoremap <C-c> "+y
vnoremap <C-x> "+x
inoremap <C-v> <Esc>"+pa
" noremap <C-c> V"+y
" nnoremap <C-v> "+P
" vnoremap <C-v> "_c<Esc>"+p

" make 'd' delete stuff, instead of cutting it to clipboard/register
nnoremap d "_d
vnoremap d "_d
" do not yank replaced text when pasting in visual mode
vnoremap p "0p

" Ctrl+Arrows: move a line of text
nnoremap <silent> <C-Down> :m .+1<CR>==
nnoremap <silent> <C-Up> :m .-2<CR>==
inoremap <silent> <C-Down> <Esc>:m .+1<CR>==gi
inoremap <silent> <C-Up> <Esc>:m .-2<CR>==gi
vnoremap <silent> <C-Down> :m '>+1<CR>gv=gv
vnoremap <silent> <C-Up> :m '<-2<CR>gv=gv

" indent block in visual mode with <Tab> (and reverse)
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

" Search for selected text by hitting <*>
vnoremap * y/<C-R>"<CR>N

" Use Enter to clear search highlighting
nnoremap <silent> <Enter> :noh<CR>

" Prevent irreversible text deletion with Ctrl+u/w
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

" Ctrl+b/n to switch between buffers
nnoremap <silent> <leader>n :bnext<CR>
nnoremap <silent> <leader>p :bprev<CR>
nnoremap <silent> ä :bnext<CR>
nnoremap <silent> ö :bprev<CR>

" open or reload .vimrc with <leader>, and <leader>,,
noremap <silent> <leader>, :e ~/.vimrc<CR>
noremap <silent> <leader>,, :source ~/.vimrc<CR>

" Use <Tab> to select entries in the autocomplete popup-menu
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Surround selection with brackets using vim-surround
vnoremap ( S)
vnoremap [ S]
vnoremap { S}
vnoremap " S"
vnoremap ' S'
vnoremap < S>

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>z :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>Z :wincmd =<cr>

" Ctrl+Alt+c to toggle Diff mode in a split view
nnoremap <silent> <C-M-c> :call ToggleDiff()<CR>
function! ToggleDiff ()
    if (&diff)
        diffoff!
    else
        windo :diffthis
    endif
endfunction
" Adjust diff colors
hi DiffAdd      gui=none    guifg=NONE          guibg=#113A0D
hi DiffDelete   gui=none    guifg=NONE          guibg=#5D161B
hi DiffChange   gui=none    guifg=NONE          guibg=NONE
hi DiffText     gui=none    guifg=NONE          guibg=#174350

" Disable Ex mode and command line mode - I never use it
nnoremap Q <nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Autocmds: do stuff automatically
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
            \ quit | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
            \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" automatically start insert mode when opening a git commit message
autocmd FileType gitcommit exec 'au VimEnter * startinsert'
