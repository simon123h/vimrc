

"    General 
" ------------

" Disable downwards compitability to Vi, could otherwise introduce errors
set nocompatible

" Set encoding
set encoding=utf-8

" Highlight matching brace
set showmatch	

" Prompt confirmation dialogs
set confirm

" Number of undo levels
set history=100
set undolevels=1000

" Enable syntax highlighting
syntax enable

" enable filetype detection
filetype plugin on

" Show row and column ruler information
set ruler

" Configure menu for command-line completion
set wildmenu
set wildmode=list:longest,full

" Enable use of the mouse for all modes
set mouse=a

" set theme for a dark background
set background=dark


"     Behaviour
"  --------------

" jump to next line if line end is reached
set whichwrap+=<,>,h,l,[,]

" Fix Backspace behaviour
set backspace=indent,eol,start

" Searches for strings incrementally
set incsearch

" Auto-indent new lines
set autoindent
set smartindent
" Enable smart tabs-spaces recognition
set smarttab 

" Do not create .swp files or backups
set noswapfile
set nobackup
set nowritebackup


"     Keybindings
"  ----------------

" remap jj to esc key for quick usage
imap jj <Esc>
vmap jj <Esc>

" Use <C-L> to clear the search highlighting
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" Prevent irreversible text deletion with Ctrl+u/w
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
